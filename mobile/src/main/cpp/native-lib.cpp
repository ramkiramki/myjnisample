#include <jni.h>
#include <string>
#include "native-lib.h"
#include "Application.h"

static Application* s_pApplication = NULL;

extern "C" {

static JavaVM *gJavaVM = 0;
static jclass classOfCplus = 0;
JNIEnv *env = 0;
char* className = (char*)"com/example/capgemini/myjnisample/JNILib";
jint JNI_OnLoad(JavaVM *vm, void *reserved)
{

    gJavaVM = vm;
    return JNI_VERSION_1_4;
}
static jmethodID getMethodID(const char *methodName, const char *paramCode)
{
    jmethodID ret = 0;

    if (gJavaVM->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK)
    {
        LOGV("Failed to get the environment using GetEnv()");
        return 0;
    }

    if (gJavaVM->AttachCurrentThread(&env, 0) < 0)
    {
        LOGV("Failed to get the environment using AttachCurrentThread()");
        return 0;
    }

    classOfCplus = env->FindClass(className);
    if (! classOfCplus)
    {
        LOGV("Give the correct Class Path");
        return 0;
    }

    if (env != 0 && classOfCplus != 0)
    {
        ret = env->GetStaticMethodID(classOfCplus, methodName, paramCode);
    }

    if (! ret)
    {
        LOGV("get method id of %s error", methodName);
    }

    return ret;
}

void OnInit()
{
    LOGV("ONINIT........");
    jmethodID endMethodID = getMethodID("OnInit", "()V");
    if (endMethodID)
    {
        env->CallStaticVoidMethod(classOfCplus, endMethodID);
    }
}
void OnResize(int iWidth, int iHeight)
{
    jmethodID endMethodID = getMethodID("OnResize", "(II)V");
    if (endMethodID)
    {
        env->CallStaticVoidMethod(classOfCplus, endMethodID,iWidth,iHeight);
    }

}
void OnFrame() {
    jmethodID endMethodID = getMethodID("OnFrame", "()V");
    if (endMethodID)
    {
        env->CallStaticVoidMethod(classOfCplus, endMethodID);
    }
}
void OnShutdown() {
    jmethodID endMethodID = getMethodID("OnShutdown", "()V");
    if (endMethodID)
    {
        env->CallStaticVoidMethod(classOfCplus, endMethodID);
    }
}
void OnTouch(int iPointerID, float fPosX,
             float fPosY, int iAction)
{
    jmethodID endMethodID = getMethodID("OnResize", "(IFFI)V");
    if (endMethodID)
    {
        env->CallStaticVoidMethod(classOfCplus, endMethodID,iPointerID,fPosX, fPosY,iAction);
    }

}
void OnPause() {
    jmethodID  endMethodID = getMethodID("OnPause","()V");
    if (endMethodID) {
        env->CallStaticVoidMethod(classOfCplus, endMethodID);

    }
}
JNIEXPORT void JNICALL
Java_com_example_capgemini_myjnisample_JNILib_OnInit(JNIEnv *env, jclass type) {

    LOGE("Hello Init!");
    LOGV("Init called.");
    if (!s_pApplication) {
        s_pApplication = new Application();
    }

    s_pApplication->OnContextCreated();

}
JNIEXPORT void JNICALL
Java_com_example_capgemini_myjnisample_JNILib_OnResize(JNIEnv *env, jclass type, jint iWidth,
                                                       jint iHeight) {

    LOGE("Hello Screen! Width: %i, Height: %i", iWidth, iHeight);
    s_pApplication->OnWindowResize(iWidth, iHeight);

}
JNIEXPORT void JNICALL
Java_com_example_capgemini_myjnisample_JNILib_OnFrame(JNIEnv *env, jclass type) {

    LOGE("Hello Frame!");
    s_pApplication->Step();
}
JNIEXPORT void JNICALL
Java_com_example_capgemini_myjnisample_JNILib_OnShutdown(JNIEnv *env, jclass type) {

    if (s_pApplication) {
        delete s_pApplication;
        s_pApplication = NULL;
    }

}
JNIEXPORT void JNICALL
Java_com_example_capgemini_myjnisample_JNILib_OnTouch(JNIEnv *env, jclass type, jint iPointerID,
                                                      jfloat fPosX, jfloat fPosY, jint iAction) {

    LOGV("Touch: %i, x: %f y:, %f action:, %i.", iPointerID, fPosX, fPosY, iAction);
    if (s_pApplication) {
        s_pApplication->OnTouch(iPointerID, fPosX, fPosY, iAction);
    }
}
JNIEXPORT void JNICALL
Java_com_example_capgemini_myjnisample_JNILib_OnPause(JNIEnv *env, jclass type) {

    if (s_pApplication) {
        s_pApplication->OnPause();
    }
}
JNIEXPORT void JNICALL
Java_com_example_capgemini_myjnisample_JNILib_OnResume(JNIEnv *env, jclass type) {

    if (s_pApplication) {
        s_pApplication->OnResume();
    }

 }
}