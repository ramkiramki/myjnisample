//
// Created by Capgemini on 27/08/18.
//

#ifndef HELLOJNI_RENDERERES2_H
#define HELLOJNI_RENDERERES2_H

#include <EGL/egl.h>

class RendererES2
{
public:
    enum ContextState
    {
        Initialised = 0,
        Invalid,
        Restored,

        RenderStateCount
    };

    RendererES2();
    ~RendererES2();

    ContextState OnContextCreated();
    void SetViewport( int iWidth, int iHeight );

    void ClearScreen( float fRed, float fGreen, float fBlue, float fAlpha = 0.0f, bool bClearDepth = true );

private:
    EGLContext m_EglContext;
};

#endif //HELLOJNI_RENDERERES2_H
