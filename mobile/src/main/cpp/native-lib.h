//
// Created by Capgemini on 20/08/18.
//

#ifndef MYJNISAMPLE_NATIVE_LIB_H
#define MYJNISAMPLE_NATIVE_LIB_H

#include <jni.h>
#include <android/log.h>

#define DEBUG 1

#define LOG_TAG "JNICHECK"
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#if DEBUG
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)

#else
#define LOGV(...)
#endif

#include "Application.h"



#endif //MYJNISAMPLE_NATIVE_LIB_H
