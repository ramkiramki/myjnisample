package com.example.capgemini.myjnisample;

public class JNILib {

    static{
        try{
            System.loadLibrary("native-lib");
            System.out.println("Loaded native-lib");
        }catch(UnsatisfiedLinkError e){
            //nothing to do
            System.out.println("Couldn't load native-lib");
            System.out.println(e.getMessage());
        }
    }

    public static native void OnInit();
    public static native void OnResize( int iWidth, int iHeight );
    public static native void OnFrame();

    public static native void OnShutdown();
    public static native void OnTouch( int iPointerID, float fPosX, float fPosY, int iAction );
    public static native void OnPause();
    public static native void OnResume();

}
